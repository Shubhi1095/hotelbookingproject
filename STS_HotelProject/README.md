# Fullstack-Hotel-Project-using-Spring-Boot-and-Angular
A Hotel Reservation Web Application Using Spring Boot and Angular


The application enables users to:

View available rooms
Create a reservation
Update a reservation
Delete a reservation 
...
