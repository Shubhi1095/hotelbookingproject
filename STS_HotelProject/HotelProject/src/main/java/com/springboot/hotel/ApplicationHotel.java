package com.springboot.hotel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.springboot.hotel")
public class ApplicationHotel {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationHotel.class, args);

	}
}
