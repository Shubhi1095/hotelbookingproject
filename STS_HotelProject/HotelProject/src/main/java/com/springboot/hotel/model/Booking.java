package com.springboot.hotel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BOOKING_TBL")
public class Booking {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	int id;
	
	@Column(name="hotel_id")
	int hotel_id;
	
	@Column(name = "hotel_name")
	String hotel_name;

	@Column(name = "check_in_date")
	String check_in_date;
	
	@Column(name = "check_out_date")
	String check_out_date;

	@Column(name = "no_of_guest")
	int no_of_guest;

	@Column(name = "no_of_rooms")
	int no_of_rooms;

	@Column(name = "user_name")
	String user_name;

	@Column(name = "booking_date")
	String booking_date;

	@Column(name = "booking_status")
	String booking_status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getHotel_id() {
		return hotel_id;
	}

	public void setHotel_id(int hotel_id) {
		this.hotel_id = hotel_id;
	}

	public String getHotel_name() {
		return hotel_name;
	}

	public void setHotel_name(String hotel_name) {
		this.hotel_name = hotel_name;
	}

	public String getCheck_in_date() {
		return check_in_date;
	}

	public void setCheck_in_date(String check_in_date) {
		this.check_in_date = check_in_date;
	}

	public String getCheck_out_date() {
		return check_out_date;
	}

	public void setCheck_out_date(String check_out_date) {
		this.check_out_date = check_out_date;
	}

	public int getNo_of_guest() {
		return no_of_guest;
	}

	public void setNo_of_guest(int no_of_guest) {
		this.no_of_guest = no_of_guest;
	}

	public int getNo_of_rooms() {
		return no_of_rooms;
	}

	public void setNo_of_rooms(int no_of_rooms) {
		this.no_of_rooms = no_of_rooms;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getBooking_date() {
		return booking_date;
	}

	public void setBooking_date(String booking_date) {
		this.booking_date = booking_date;
	}

	public String getBooking_status() {
		return booking_status;
	}

	public void setBooking_status(String booking_status) {
		this.booking_status = booking_status;
	}

	@Override
	public String toString() {
		return "Booking [id=" + id + ", hotel_id=" + hotel_id + ", hotel_name=" + hotel_name + ", check_in_date="
				+ check_in_date + ", check_out_date=" + check_out_date + ", no_of_guest=" + no_of_guest
				+ ", no_of_rooms=" + no_of_rooms + ", user_name=" + user_name + ", booking_date=" + booking_date
				+ ", booking_status=" + booking_status + "]";
	}
}
