package com.springboot.hotel.service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.hotel.dao.BookingRepository;
import com.springboot.hotel.dao.HotelRepository;
import com.springboot.hotel.model.Booking;
import com.springboot.hotel.model.Hotel;

@Service
public class HotelService {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	HotelRepository dao;
	
	@Autowired
	BookingRepository bookingdao;
	
	public List<Hotel> getData() {
		return dao.findAll().stream().sorted(Comparator.comparing(Hotel::getReviews_rating))
				.collect(Collectors.toList());
	}
	
	public List<Hotel> getFilteredData(String city) {
		/*
		 * fetch hotels by city 
		 */
		return dao.findAll().stream().filter(cityfil -> cityfil.getCity().equalsIgnoreCase(city))
				.collect(Collectors.toList());
	}

	public int bookingData(Booking data) {
	 bookingdao.save(data);
	 return 1;
		
	}
	
	public void addHotelData(Hotel data) {
		 dao.save(data);
		}
	
	public Optional<Hotel> getDataById(int id) {
		
		return dao.findById(id);
	}

	public void delete(Integer id) {
		// TODO Auto-generated method stub
		dao.deleteById(id);
	}
	
	public void updateHotel(Hotel data) {
		
		dao.saveAndFlush(data);
	}
}
