package com.springboot.hotel.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.springboot.hotel.model.Booking;

public interface BookingRepository extends JpaRepository<Booking, Integer> {

}
