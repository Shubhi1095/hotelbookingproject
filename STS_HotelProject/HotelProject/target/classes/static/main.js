(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing/app-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/app-routing/app-routing.module.ts ***!
  \***************************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _homepage_component_homepage_component_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../homepage-component/homepage-component.component */ "./src/app/homepage-component/homepage-component.component.ts");
/* harmony import */ var _bookingpage_bookingpage_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../bookingpage/bookingpage.component */ "./src/app/bookingpage/bookingpage.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        component: _homepage_component_homepage_component_component__WEBPACK_IMPORTED_MODULE_2__["HomepageComponentComponent"]
    },
    {
        path: 'home',
        component: _homepage_component_homepage_component_component__WEBPACK_IMPORTED_MODULE_2__["HomepageComponentComponent"]
    },
    {
        path: 'bookhotel/:id',
        component: _bookingpage_bookingpage_component__WEBPACK_IMPORTED_MODULE_3__["BookingpageComponent"]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'hotelproject';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_routing_app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing/app-routing.module */ "./src/app/app-routing/app-routing.module.ts");
/* harmony import */ var _homepage_component_homepage_component_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./homepage-component/homepage-component.component */ "./src/app/homepage-component/homepage-component.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var angular_paginator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-paginator */ "./node_modules/angular-paginator/fesm5/angular-paginator.js");
/* harmony import */ var mydatepicker__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! mydatepicker */ "./node_modules/mydatepicker/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _bookingpage_bookingpage_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./bookingpage/bookingpage.component */ "./src/app/bookingpage/bookingpage.component.ts");
/* harmony import */ var _bookingsuccess_bookingsuccess_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./bookingsuccess/bookingsuccess.component */ "./src/app/bookingsuccess/bookingsuccess.component.ts");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/dialog */ "./node_modules/primeng/dialog.js");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(primeng_dialog__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var primeng_primeng__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/primeng */ "./node_modules/primeng/primeng.js");
/* harmony import */ var primeng_primeng__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(primeng_primeng__WEBPACK_IMPORTED_MODULE_14__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















// import { DropdownModule, FileUploadModule, ConfirmDialogModule, CalendarModule } from 'primeng/primeng';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _homepage_component_homepage_component_component__WEBPACK_IMPORTED_MODULE_5__["HomepageComponentComponent"],
                _bookingpage_bookingpage_component__WEBPACK_IMPORTED_MODULE_11__["BookingpageComponent"],
                _bookingsuccess_bookingsuccess_component__WEBPACK_IMPORTED_MODULE_12__["BookingsuccessComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                _app_routing_app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HttpClientModule"],
                angular_paginator__WEBPACK_IMPORTED_MODULE_8__["AngularPaginatorModule"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_7__["NgxPaginationModule"],
                primeng_dialog__WEBPACK_IMPORTED_MODULE_13__["DialogModule"],
                mydatepicker__WEBPACK_IMPORTED_MODULE_9__["MyDatePickerModule"],
                // DropdownModule,
                primeng_primeng__WEBPACK_IMPORTED_MODULE_14__["ConfirmDialogModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_14__["SharedModule"]
            ],
            providers: [primeng_primeng__WEBPACK_IMPORTED_MODULE_14__["ConfirmationService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/bookingpage/bookingpage.component.html":
/*!********************************************************!*\
  !*** ./src/app/bookingpage/bookingpage.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n  \n  <div *ngIf=\"h\">\n      <h2  class=\"heading\">Welcome to {{h.name}}</h2>\n  </div>\n\n\n      <form id=\"mainForm\">\n          <div id=\"in\" style=\"width: 15%\">Check In: <my-date-picker [(ngModel)]=\"date1\" [ngModelOptions]=\"{standalone: true}\"></my-date-picker></div>\n          <div id=\"out\" style=\"width: 15%\">Check Out:<my-date-picker [(ngModel)]=\"date2\" [ngModelOptions]=\"{standalone: true}\" ></my-date-picker></div>\n          <div id=\"guests\"> No of Guests: <input type=\"number\" name=\"noOfGuests\" id=\"noOfGuests\" min=\"1\" value=1 [(ngModel)]=\"noOfGuests\" [ngModelOptions]=\"{standalone: true}\" required ></div>\n          <div id=\"rooms\"> No of Rooms: <input type=\"number\" name=\"noOfRooms\" id=\"noOfRooms\" min=\"1\" value=1 [(ngModel)]=\"noOfRooms\" [ngModelOptions]=\"{standalone: true}\" required></div>\n          <button (click) = \"submit(h.name,h.id,date1,date2,noOfGuests,noOfRooms)\" id=\"submitButton\">Confirm Booking</button>\n          <div *ngIf=\"created==1\"><app-bookingsuccess></app-bookingsuccess></div>\n          <button (click) = \"goBackHome()\" id=\"backButton\">Book New Hotel</button>\n      </form>\n\n\n  \n\n\n  \n\n"

/***/ }),

/***/ "./src/app/bookingpage/bookingpage.component.scss":
/*!********************************************************!*\
  !*** ./src/app/bookingpage/bookingpage.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/bookingpage/bookingpage.component.ts":
/*!******************************************************!*\
  !*** ./src/app/bookingpage/bookingpage.component.ts ***!
  \******************************************************/
/*! exports provided: BookingpageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookingpageComponent", function() { return BookingpageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _hotel_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../hotel.service */ "./src/app/hotel.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BookingpageComponent = /** @class */ (function () {
    function BookingpageComponent(route, hotelService, r) {
        this.route = route;
        this.hotelService = hotelService;
        this.r = r;
    }
    BookingpageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (res) { _this.hotelId = res.id; });
        console.log('hotel id = ' + this.hotelId);
        this.url = 'http://localhost:8080/HotelManagement/rest/hotels/' + this.hotelId;
        console.log(this.url);
        this.hotelService.doGet(this.url).subscribe(function (d) {
            _this.h = d.responseBody;
            console.log(_this.h);
        });
    };
    BookingpageComponent.prototype.goBackHome = function () {
        this.r.navigate(['home']);
    };
    BookingpageComponent.prototype.submit = function (name, id, d1, d2, g, rms) {
        var _this = this;
        if (d1 !== null && d1 !== undefined) {
            d1 = JSON.parse(JSON.stringify(d1)).formatted;
        }
        if (d2 !== null && d2 !== undefined) {
            d2 = JSON.parse(JSON.stringify(d2)).formatted;
        }
        console.log('check in:' + d1);
        console.log('check out:' + d2);
        console.log('guests' + g);
        console.log('rooms:' + rms);
        if (d1 == null || d2 == null || g == null || rms == null) {
            alert('Please make sure all details are filled Properly! Thank You');
        }
        else if (d2 < d1) {
            alert('Please make sure check out time is larger than check in time! Thank You');
        }
        else if (g <= 0 || rms <= 0) {
            alert('Please make sure no of guests or no of rooms >=1! Thank You');
        }
        else if (this.h.availabilty < rms) {
            alert('Sorry Rooms are not available!');
        }
        else {
            this.bookingOrder = {
                hotel_id: id, user_name: '', hotel_name: '', check_in_date: null, check_out_date: null,
                no_of_guest: 0, no_of_rooms: 0, booking_date: null, booking_status: ''
            };
            this.bookingOrder.check_in_date = d1.toString();
            this.bookingOrder.check_out_date = d2.toString();
            this.bookingOrder.no_of_guest = g;
            this.bookingOrder.no_of_rooms = rms;
            this.bookingOrder.booking_status = 'booked';
            this.bookingOrder.hotel_name = name;
            this.bookingOrder.booking_date = new Date().toString();
            this.bookingOrder.hotel_id = id;
            this.bookingOrder.user_name = 'shubhi@test';
            console.log(this, this.bookingOrder);
            this.url = 'http://localhost:8080/HotelManagement/rest/HotelService/booking/create';
            // calling the create booking request api
            this.hotelService.doPost(this.url, this.bookingOrder).subscribe(function (response) {
                console.log('PUT Request is successful ', response);
                _this.bookingResponse = response;
                _this.created = _this.bookingResponse;
                console.log('Booking Response:' + _this.created);
            }, function (error) {
                console.log('Error', error);
            });
        }
    };
    BookingpageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bookingpage',
            template: __webpack_require__(/*! ./bookingpage.component.html */ "./src/app/bookingpage/bookingpage.component.html"),
            styles: [__webpack_require__(/*! ./bookingpage.component.scss */ "./src/app/bookingpage/bookingpage.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _hotel_service__WEBPACK_IMPORTED_MODULE_2__["HotelService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], BookingpageComponent);
    return BookingpageComponent;
}());



/***/ }),

/***/ "./src/app/bookingsuccess/bookingsuccess.component.html":
/*!**************************************************************!*\
  !*** ./src/app/bookingsuccess/bookingsuccess.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p id=\"booking-success-message\"> Your booking order is successfully placed, Thank you for visiting! Click Below for More Hotels!</p>\n"

/***/ }),

/***/ "./src/app/bookingsuccess/bookingsuccess.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/bookingsuccess/bookingsuccess.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/bookingsuccess/bookingsuccess.component.ts":
/*!************************************************************!*\
  !*** ./src/app/bookingsuccess/bookingsuccess.component.ts ***!
  \************************************************************/
/*! exports provided: BookingsuccessComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookingsuccessComponent", function() { return BookingsuccessComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BookingsuccessComponent = /** @class */ (function () {
    function BookingsuccessComponent() {
    }
    BookingsuccessComponent.prototype.ngOnInit = function () {
    };
    BookingsuccessComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bookingsuccess',
            template: __webpack_require__(/*! ./bookingsuccess.component.html */ "./src/app/bookingsuccess/bookingsuccess.component.html"),
            styles: [__webpack_require__(/*! ./bookingsuccess.component.scss */ "./src/app/bookingsuccess/bookingsuccess.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BookingsuccessComponent);
    return BookingsuccessComponent;
}());



/***/ }),

/***/ "./src/app/homepage-component/homepage-component.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/homepage-component/homepage-component.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/homepage-component/homepage-component.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/homepage-component/homepage-component.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"heading\">Welcome</h1>\n<div class=\"input-group\">\n  <input type=\"text\" class=\"form-control\" id=\"search-input\" placeholder=\"Search Hotel by City..\" [(ngModel)]=\"city\"\n    (keyup)=\"searchHotelsByCity()\">\n\n    <button (click)=\"showDialogToAdd()\" class=\"book-button\"> Add Hotel </button>\n</div>\n\n\n<div *ngFor=\"let hotel of hotels | angularPaginator: { currentPage: currentPage }; let i = index\"\n  style=\"background-color:lightblue\">\n  <div class=\"hotel\">\n    <h3>{{hotel.name}}</h3>\n    <div class=\"hotel-info\">\n      <p>{{hotel.city}}, {{hotel.country}}</p>\n      <p>Ratings: {{hotel.reviews_rating}} / 10</p>\n      <p>Reviews: {{hotel.reviews_text}}</p>\n    </div>\n    <button (click)=\"bookHotel(hotel.id)\" class=\"book-button\"> Book Now </button>\n    <button (click)=\"updateHotel(hotel)\" class=\"book-button\"> Update </button>\n  </div>\n</div>\n\n<p-dialog header = {{dialogHeader}} [(visible)]=\"displayDialog\" [draggable]=\"true\" [autoZIndex]=\"true\" [responsive]=\"true\"\n  showEffect=\"fade\" [modal]=\"true\" [style]=\"{'background':'#f2f2f2'}\" [width]=\"600\">\n  <form name=\"form\" (ngSubmit)=\"f.form.valid\" #f=\"ngForm\" novalidate>\n    <div class=\"ui-g ui-fluid\" *ngIf=\"uhotel\">\n      <p>Please fill out Required Fields *</p>\n      <div class=\"ui-g-12\" [hidden] = true>\n        <div class=\"ui-g-6\">\n          <label for=\"id\">* ID</label>\n        </div>\n        <div class=\"ui-g-6\">\n          <input pInputText id=\"id\" name=\"id\" [(ngModel)]=\"uhotel.id\" #id=\"ngModel\"/>\n        </div>\n      </div>\n      <div class=\"ui-g-12\">\n        <div class=\"ui-g-2\">\n          <label for=\"name\">*Hotel Name</label>\n        </div>\n        <div class=\"ui-g-4\">\n          <input pInputText id=\"name\" name=\"name\" [(ngModel)]=\"uhotel.name\" #name=\"ngModel\" required />\n        </div>\n\n        <div class=\"ui-g-2\">\n          <label for=\"country\">*Country</label>\n        </div>\n        <div class=\"ui-g-4\">\n          <input pInputText id=\"country\" name=\"country\" [(ngModel)]=\"uhotel.country\" #country=\"ngModel\" required />\n        </div>\n      </div>\n\n      <div class=\"ui-g-12\">\n        <div class=\"ui-g-2\">\n          <label for=\"address\">*Address</label>\n        </div>\n        <div class=\"ui-g-4\">\n          <input pInputText id=\"address\" name=\"address\" [(ngModel)]=\"uhotel.address\" #address=\"ngModel\" required />\n        </div>\n\n        <div class=\"ui-g-2\">\n          <label for=\"availabilty\">*Availabilty</label>\n        </div>\n        <div class=\"ui-g-4\">\n          <input type=\"number\" min=\"1\" value=1 pInputText id=\"availabilty\" name=\"availabilty\" [(ngModel)]=\"uhotel.availabilty\" #availabilty=\"ngModel\" required />\n        </div>\n      </div>\n\n      <div class=\"ui-g-12\">\n        <div class=\"ui-g-2\">\n          <label for=\"city\">*City</label>\n        </div>\n        <div class=\"ui-g-4\">\n          <input pInputText id=\"city\" name=\"city\" [(ngModel)]=\"uhotel.city\" #city=\"ngModel\" required />\n        </div>\n\n        <div class=\"ui-g-2\">\n          <label for=\"reviews_rating\">*Reviews Rating</label>\n        </div>\n        <div class=\"ui-g-4\">\n          <input type=\"number\" min=\"1\" value=1 pInputText id=\"reviews_rating\" name=\"reviews_rating\" [(ngModel)]=\"uhotel.reviews_rating\" #reviews_rating=\"ngModel\" required />\n        </div>\n      </div>\n\n      <div class=\"ui-g-12\">\n        <div class=\"ui-g-2\">\n          <label for=\"reviews_text\">*Review Comments</label>\n        </div>\n        <div class=\"ui-g-4\">\n          <input pInputText id=\"reviews_text\" name=\"reviews_text\" [(ngModel)]=\"uhotel.reviews_text\" #reviews_text=\"ngModel\" />\n        </div>\n      </div>\n    </div>\n    \n      <div class=\"ui-dialog-buttonpane ui-helper-clearfix\">\n        <button type=\"button\" (click)=\"delete()\" class=\"book-button\">Delete</button>\n        <button type=\"button\" (click)=\"save()\" class=\"book-button\"\n          [disabled]=\"f.form.pristine || f.form.invalid\">Save</button>\n      </div>\n    \n  </form>\n</p-dialog>\n\n<app-angular-paginator (pageChange)=\"currentPage = $event\" className=\"paginator\"></app-angular-paginator>"

/***/ }),

/***/ "./src/app/homepage-component/homepage-component.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/homepage-component/homepage-component.component.ts ***!
  \********************************************************************/
/*! exports provided: HomepageComponentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomepageComponentComponent", function() { return HomepageComponentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _hotel_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../hotel.service */ "./src/app/hotel.service.ts");
/* harmony import */ var primeng_components_common_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/components/common/api */ "./node_modules/primeng/components/common/api.js");
/* harmony import */ var primeng_components_common_api__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(primeng_components_common_api__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var primeng_primeng__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/primeng */ "./node_modules/primeng/primeng.js");
/* harmony import */ var primeng_primeng__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(primeng_primeng__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomepageComponentComponent = /** @class */ (function () {
    function HomepageComponentComponent(confirmService, hotelService, route) {
        this.confirmService = confirmService;
        this.hotelService = hotelService;
        this.route = route;
        this.p = 1;
        this.errorOccured = false;
        this.userResponse = [];
        this.displayDialog = false;
    }
    HomepageComponentComponent.prototype.ngOnInit = function () {
        this.url = 'http://localhost:8080/HotelManagement/rest/hotels';
        this.hotels = [];
        this.uhotel = this.addNewHotel();
        this.dialogHeader = '';
        this.getHotels();
        this.newhotel = false;
    };
    HomepageComponentComponent.prototype.addNewHotel = function () {
        console.log('New Hotel >>>>');
        var newObj = {
            id: 0,
            name: '',
            country: '',
            address: '',
            availabilty: 0,
            city: '',
            reviews_rating: 0,
            reviews_text: '',
            reviews_username: ''
        };
        return newObj;
    };
    HomepageComponentComponent.prototype.getHotels = function () {
        var _this = this;
        this
            .hotelService
            .doGet(this.url)
            .subscribe(function (data) {
            _this.hotels = data.resBodyList;
        });
    };
    HomepageComponentComponent.prototype.bookHotel = function (id) {
        this.route.navigate(['bookhotel/' + id]);
    };
    HomepageComponentComponent.prototype.updateHotel = function (hotel) {
        console.log('update hotel:' + hotel.name);
        this.dialogHeader = 'Edit Hotel';
        this.displayDialog = true;
        this.uhotel = hotel;
        this.newhotel = false;
    };
    HomepageComponentComponent.prototype.save = function () {
        var _this = this;
        this
            .hotelService
            .updateHotel(this.uhotel)
            .subscribe(function (data) {
            _this.getHotels();
            _this.displayDialog = false;
            _this.uhotel = null;
        });
    };
    HomepageComponentComponent.prototype.showDialogToAdd = function () {
        this.displayDialog = true;
        this.uhotel = this.addNewHotel();
        this.selectedHotel = null;
        this.dialogHeader = 'Add Hotel';
        this.newhotel = true;
    };
    HomepageComponentComponent.prototype.delete = function () {
        console.log('Inside delete' + this.uhotel.name);
        this.deleteBatch(this.uhotel);
        // this.confirmService.confirm({
        //   message: ` Are You Confirm  ?`,
        //   header: 'Remove User Detail',
        //   icon: 'fa fa-question-circle',
        //   accept: () => {
        //     this.userResponse = [];
        //     this.deleteBatch(this.uhotel);
        //     this.userResponse.push({
        //       severity: 'info', summary: 'Confirmed',
        //       detail: 'I like PrimeNG'
        //     });
        //   },
        //   reject: () => {
        //     this.userResponse = [];
        //     this.userResponse.push({
        //       severity: 'info', summary: 'Rejected',
        //       detail: 'I don\'t really like PrimeNG'
        //     });
        //   }
        // });
    };
    HomepageComponentComponent.prototype.deleteBatch = function (hotel) {
        var _this = this;
        console.log('<<<<Selected Hotel id: ' + hotel.id);
        if (hotel.id !== undefined || hotel.id !== null) {
            this.hotelService.delete(hotel.id)
                .subscribe(function (result) {
                _this.getHotels();
                _this.uhotel = null;
                _this.displayDialog = false;
            }, function (error) {
            });
        }
    };
    HomepageComponentComponent.prototype.searchHotelsByCity = function () {
        var _this = this;
        this.hotels = [];
        this.url = 'http://localhost:8080/HotelManagement/rest/HotelService/filter/hotels/search';
        this.hotels = [];
        console.log(this.city.length);
        if (this.city.length === 0) {
            this.url = 'http://localhost:8080/HotelManagement/rest/hotels';
            this.hotelService.doGet(this.url).subscribe(function (data) {
                _this.hotels = data.resBodyList;
            });
        }
        else {
            this
                .hotelService
                .getHotelsByCity(this.url, this.city)
                .subscribe(function (data) {
                _this.hotels = data.resBodyList;
            }, function (error) {
                _this.errorOccured = true;
                _this.err = error;
                console.log(_this.err);
            });
        }
    };
    HomepageComponentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-homepage-component',
            template: __webpack_require__(/*! ./homepage-component.component.html */ "./src/app/homepage-component/homepage-component.component.html"),
            styles: [__webpack_require__(/*! ./homepage-component.component.css */ "./src/app/homepage-component/homepage-component.component.css")],
            providers: [primeng_components_common_api__WEBPACK_IMPORTED_MODULE_3__["ConfirmationService"], primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]]
        }),
        __metadata("design:paramtypes", [primeng_components_common_api__WEBPACK_IMPORTED_MODULE_3__["ConfirmationService"],
            _hotel_service__WEBPACK_IMPORTED_MODULE_2__["HotelService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], HomepageComponentComponent);
    return HomepageComponentComponent;
}());



/***/ }),

/***/ "./src/app/hotel.service.ts":
/*!**********************************!*\
  !*** ./src/app/hotel.service.ts ***!
  \**********************************/
/*! exports provided: HotelService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HotelService", function() { return HotelService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HotelService = /** @class */ (function () {
    function HotelService(http) {
        this.http = http;
        this.url = src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api_root + '/HotelManagement/rest/hotels';
        this.deleteHotel = src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api_root + '/HotelManagement/delete';
        this.updateHotelinfo = src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api_root + '/HotelManagement/update';
        this.addHotelinfo = src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].api_root + '/HotelManagement/add';
    }
    HotelService.prototype.getHotels = function () {
        return this
            .http
            .get(this.url);
    };
    HotelService.prototype.doGet = function (u) {
        this.url = u;
        return this.http.get(this.url);
    };
    HotelService.prototype.doPost = function (u, b) {
        this.url = u;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json', 'Accept': 'application/json', 'Access-Control-Allow-Origin': 'true' });
        return this.http.post(this.url, {
            'hotel_id': b.hotel_id,
            'hotel_name': b.hotel_name,
            'check_in_date': b.check_in_date,
            'check_out_date': b.check_out_date,
            'no_of_guest': b.no_of_guest,
            'no_of_rooms': b.no_of_rooms,
            'user_name': b.user_name,
            'booking_date': b.booking_date,
            'booking_status': b.booking_status
        }, { headers: headers });
    };
    HotelService.prototype.getHotelsByCity = function (u, city) {
        this.url = u;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
        params = params.append('city', city);
        return this.http.get(this.url, {
            params: params
        });
    };
    HotelService.prototype.delete = function (id) {
        console.log('Delete Hotel info with Id: ' + id);
        return this.http.get(this.deleteHotel + '/' + id);
    };
    HotelService.prototype.updateHotel = function (updateData) {
        return this.http.patch(this.updateHotelinfo, updateData).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (data) { return console.log('All: ' + JSON.stringify(data)); }));
    };
    HotelService.prototype.addNewHotel = function (newData) {
        return this.http.post(this.addHotelinfo, newData);
    };
    HotelService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], HotelService);
    return HotelService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    profile: 'DEV',
    api_root: 'http://localhost:8080'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\ShubhiGarg\AngularHotel\hotelproject\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map