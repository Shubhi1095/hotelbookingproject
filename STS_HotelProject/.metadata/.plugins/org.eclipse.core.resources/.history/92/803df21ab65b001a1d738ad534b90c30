package com.springboot.hotel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.hotel.dao.HotelRepository;
import com.springboot.hotel.model.Hotel;
import com.springboot.hotel.service.HotelService;
import com.springboot.hotel.util.IModel;
import com.springboot.hotel.util.ResponseModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/HotelManagement")
public class HotelManagement {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	HotelService service;
	
	@ApiOperation(value = "Get Data", response = ResponseEntity.class)
	@GetMapping("/rest/hotels")
	public ResponseEntity<IModel> getData() {

		List<Hotel> response = service.getData();
		
		return populateResponse(response, HttpStatus.OK, "Data Fetched Sucessfully");

	}
	
	private ResponseEntity<IModel> populateResponse(List<Hotel> hotel, HttpStatus status, String message) {
		ResponseModel<Hotel> respModel = new ResponseModel<Hotel>();
		respModel.setStatusCode(status.value());
		respModel.setMessage(message);
		respModel.setResBodyList(hotel);
		return new ResponseEntity<IModel>(respModel, status);
	}
}
