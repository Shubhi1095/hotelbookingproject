package com.springboot.hotel.util;

import java.util.List;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModelProperty;

/**
 * The Class ResponseModel.
 * 
 * @author Shubhi Garg
 */
@Component
@JsonInclude(com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL)
public class ResponseModel<T> implements IModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(notes = "statusCode(i.e., status code of the service response code )")
	/** The status code. */
	private int statusCode;
	@ApiModelProperty(notes = "success(i.e., values true and flase  )")
	/** The success. */
	private boolean success = true;
	@ApiModelProperty(notes = "module(i.e., Service Name )")
	/** The module. */
	private String module;
	@ApiModelProperty(notes = "message (i.e., message from service )")
	/** The message. */
	private String message;
	@ApiModelProperty(notes = "responseBody (i.e., Actual response  )")
	/** The response body. */
	private @Nullable T responseBody;

	/** The res body list. */
	private List<T> resBodyList;

	/** The res body list. */
	private T resBodyList1;
	
	/** The path. */
	private String path;

	@ApiModelProperty(notes = "error (i.e., Api error wrapper of DH Service message  )")
	private ApiError error;

	/**
	 * Gets the status code.
	 *
	 * @return the status code
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * Sets the status code.
	 *
	 * @param statusCode
	 *            the new status code
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * Checks if is success.
	 *
	 * @return true, if is success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * Sets the success.
	 *
	 * @param success
	 *            the new success
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * Gets the module.
	 *
	 * @return the module
	 */
	public String getModule() {
		return module;
	}

	/**
	 * Sets the module.
	 *
	 * @param module
	 *            the new module
	 */
	public void setModule(String module) {
		this.module = module;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message
	 *            the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the response body.
	 *
	 * @return the response body
	 */
	public T getResponseBody() {
		return responseBody;
	}

	/**
	 * Sets the response body.
	 *
	 * @param result
	 *            the new response body
	 */
	public void setResponseBody(T result) {
		this.responseBody = result;
	}

	/**
	 * Gets the res body list.
	 *
	 * @return the res body list
	 */
	public List<T> getResBodyList() {
		return resBodyList;
	}

	/**
	 * Sets the res body list.
	 *
	 * @param resBodyList
	 *            the new res body list
	 */
	public void setResBodyList(List<T> resBodyList) {
		this.resBodyList = resBodyList;
	}

	/**
	 * Gets the path.
	 *
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Sets the path.
	 *
	 * @param path
	 *            the new path
	 */
	public void setPath(String path) {
		this.path = path;
	}

	public ApiError getError() {
		return error;
	}

	public void setError(ApiError error) {
		this.error = error;
	}

	public void setResBodyList(T resBodyList1) {
		this.resBodyList1 = resBodyList1;	
	}

	@Override
	public String toString() {
		return "ResponseModel [statusCode=" + statusCode + ", success=" + success + ", module=" + module + ", message="
				+ message + ", responseBody=" + responseBody + ", resBodyList=" + resBodyList + ", resBodyList1="
				+ resBodyList1 + ", path=" + path + ", error=" + error + "]";
	}

	
}
