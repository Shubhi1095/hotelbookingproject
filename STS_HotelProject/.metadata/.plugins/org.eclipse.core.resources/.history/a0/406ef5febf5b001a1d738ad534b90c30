package com.springboot.hotel.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.hotel.model.Hotel;
import com.springboot.hotel.service.HotelService;
import com.springboot.hotel.util.IModel;
import com.springboot.hotel.util.ResponseModel;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/HotelManagement")
@CrossOrigin(origins = "http://localhost:4200")
public class HotelManagement {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	HotelService service;
	
	@ApiOperation(value = "Get Data", response = ResponseEntity.class)
	@GetMapping("/rest/hotels")
	public ResponseEntity<IModel> getData() {

		log.info("Get Hotel Data");
		List<Hotel> response = service.getData();
		
		return populateResponse(response, HttpStatus.OK, "Data Fetched Sucessfully");

	}
	
	@GetMapping("rest/HotelService/filter/hotels/search")
	public ResponseEntity<IModel> getFilteredData(@RequestParam("city") String city) {

		log.info("Get Hotel Filtered Data" + city);
		List<Hotel> response = service.getFilteredData();
		
		return populateResponse(response, HttpStatus.OK, "Filtered Data Fetched Sucessfully");

	}
	
	private ResponseEntity<IModel> populateResponse(List<Hotel> hotel, HttpStatus status, String message) {
		ResponseModel<Hotel> respModel = new ResponseModel<Hotel>();
		respModel.setStatusCode(status.value());
		respModel.setMessage(message);
		respModel.setResBodyList(hotel);
		return new ResponseEntity<IModel>(respModel, status);
	}
}
